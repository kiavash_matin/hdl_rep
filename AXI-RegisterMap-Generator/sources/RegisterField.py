from FieldValue import FieldValue


class RegisterField:

    def __init__(self, alias, range, reset):
        assert isinstance(alias, str)
        self.alias = alias
        assert isinstance(range, str)
        self.range = range
        assert isinstance(reset, str)
        self.reset = reset
        self.values : FieldValue = []

    @property
    def isValuesEmpty(self):
        if 0 == len(self.values):
            return True
        else:
            return False

    def addValue(self, value):
        assert isinstance(value, FieldValue)
        assert False == self.isActionConfilt(value)
        self.values.append(value)

    def isActionConfilt(self, value: FieldValue):
       if value.action == "x":
           return False
       for val in self.values:
           if val.action == value.action:
               return True
       return False
