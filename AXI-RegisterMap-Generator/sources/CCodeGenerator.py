import math
from jinja2 import Environment, FileSystemLoader
from FieldValue import FieldValue
from RegisterField import RegisterField
from Register import Register
from RegisterFile import RegisterFile

class CCodeGenerator:

    def __init__(self, registerFile : RegisterFile, inputDir : str, outputDir : str, inputHeaderFile : str, inputSourceFile : str):

        assert isinstance(registerFile, RegisterFile)
        self.registerFile = registerFile
        assert isinstance(inputDir, str)
        file_loader = FileSystemLoader(inputDir)
        env = Environment(loader=file_loader)
        assert isinstance(inputHeaderFile, str)
        self.headerTemplate = env.get_template(inputHeaderFile)
        assert isinstance(inputSourceFile, str)
        self.sourceTemplate = env.get_template(inputSourceFile)
        assert isinstance(outputDir, str)
        self.outputDir = outputDir

    def getMsbLsb(self, field):
        range = field.range.split(":")
        if len(range) > 1:
            return  [field.range.split(":")[0],field.range.split(":")[1]]
        else:
            return [field.range.split(":")[0],field.range.split(":")[0]]

    def getMask(self, msb, lsb):
        registerWidth = 32
        mask = ["1"]*(msb-lsb+1)
        mask.extend(["0"]*lsb)
        mask = "".join(mask)
        return hex(int(mask,2))[2:].zfill(8)

    def generateCode(self):

        regs = []
        masks = []
        shifts = []
        micros = []
        actions = []

        for idx, reg in enumerate(self.registerFile.registers):
            baseAddressDec = int(self.registerFile.baseAddress, 16)
            offsetDec = int(reg.offset, 16)
            regDict = {reg.name: hex(baseAddressDec+offsetDec)[2:].zfill(8)}
            regs.append(regDict)
            for field in reg.fields:
                [msb, lsb] = self.getMsbLsb(field)
                if  int(msb) == 31 and int(lsb) == 0:
                    continue
                mask = self.getMask(int(msb), int(lsb))
                maskDict = {"{}_{}".format(reg.name, field.alias): mask}
                masks.append(maskDict)
                shiftDict = {"{}_{}".format(reg.name, field.alias): lsb}
                shifts.append(shiftDict)
                microString = "{}_{}".format(reg.name, field.alias)
                micros.append(microString)
                for val in field.values:
                    if val.action == "x":
                        continue
                    actionDict = {"{}_{}".format(reg.name, val.action): hex(int(val.value,2))[2:].zfill(8)}
                    actions.append(actionDict)

        headerOutput = self.headerTemplate.render(offset=self.registerFile.baseAddress, name=self.registerFile.name, regs=regs,
                                      masks=masks, shifts=shifts, micros=micros, actions=actions)

        includeName = "{}.h".format(self.registerFile.name)
        sourceOutput = self.sourceTemplate.render(includeName=includeName)

        headerFilePath = "{}/{}.h".format(self.outputDir, self.registerFile.name)
        f = open(headerFilePath, "w+")
        f.write(headerOutput)
        f.close()

        sourceFilePath = "{}/{}.c".format(self.outputDir, self.registerFile.name)
        f = open(sourceFilePath, "w+")
        f.write(sourceOutput)
        f.close()
        print("C Code generated successfully..!")
