import math
from jinja2 import Environment, FileSystemLoader
from FieldValue import FieldValue
from RegisterField import RegisterField
from Register import Register
from RegisterFile import RegisterFile
from VhdlRecord import VhdlRecord

class HtmlDocGenerator:

    def __init__(self, registerFile : RegisterFile, inputDir : str, outputDir : str, inputFile : str):
        assert isinstance(registerFile, RegisterFile)
        self.registerFile = registerFile
        assert isinstance(inputDir, str)
        file_loader = FileSystemLoader(inputDir)
        env = Environment(loader=file_loader)
        assert isinstance(inputFile, str)
        self.template = env.get_template(inputFile)
        assert isinstance(outputDir, str)
        self.outputDir = outputDir

    def generateCode(self):
        output = self.template.render(entityName=self.registerFile.name, baseAddress=self.registerFile.baseAddress, registers=self.registerFile.registers)
        htmlFilePath = "{}/{}.htm".format(self.outputDir, self.registerFile.name)
        f = open(htmlFilePath, "w+")
        f.write(output)
        f.close()
        print("HTML code generated successfully..!")
