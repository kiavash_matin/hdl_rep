from RegisterField import RegisterField


class Register:

    def __init__(self, name, direction, offset, record):
        assert isinstance(name, str)
        self.name = name
        assert isinstance(direction, str)
        self.direction = direction
        assert isinstance(offset, str)
        assert 0 == int(offset, 16) % 4
        self.offset = offset
        assert isinstance(record, str)
        self.record = record
        self.fields : RegisterField = []

    @property
    def isFieldsEmpty(self):
        if 0 == len(self.fields):
            return True
        else:
            return False

    def addField(self, field : RegisterField):
        assert isinstance(field, RegisterField)
        assert False == self.doesFieldExist(field)
        self.fields.append(field);

    def doesFieldExist(self, field : RegisterField):
        for f in self.fields:
            if f.alias == field.alias:
                return True
        return False
