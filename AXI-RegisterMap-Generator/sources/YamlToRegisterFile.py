from typing import List, Any
from FieldValue import FieldValue
from RegisterField import RegisterField
from VhdlRecord import VhdlRecord
from RecordMember import RecordMember
from Register import Register
from RegisterFile import RegisterFile
from VhdlCodeGenerator import VhdlCodeGenerator
from CCodeGenerator import CCodeGenerator
import yaml

class YamlToRegisterFile:

    def __init__(self, yamlPath : str):
        assert isinstance(yamlPath, str)
        file_descriptor = open(yamlPath)
        data = yaml.load(file_descriptor)
        allRegisters = data["registerFile"]["registers"]
        self.registerFile = RegisterFile(data["registerFile"]["config"]["entityName"], data["registerFile"]["config"]["baseAddress"])
        for reg  in data["registerFile"]["registers"]:
            newReg = Register(reg["name"], reg["dir"], reg["offset"], reg["record"])
            for f in reg["fields"]:
                newField =  RegisterField(f["alias"], f["range"], f["reset"])
                for val in f["value"]:
                    newValue = FieldValue(val["val"], val["action"], val["description"])
                    newField.addValue(newValue)
                newReg.addField(newField)
            if reg["record"] != "x":
                newRecord = VhdlRecord(newReg.record, newReg.direction)
                for f in newReg.fields:
                    self.registerFile.addRecord(newRecord, RecordMember(f.alias, newReg))
            self.registerFile.addRegister(newReg)
