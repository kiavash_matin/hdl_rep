class FieldValue:
    def __init__(self, value, action, description):
        assert isinstance(value, str)
        self.value = value
        assert isinstance(description, str)
        self.description = description
        assert isinstance(action, str)
        self.action = action
