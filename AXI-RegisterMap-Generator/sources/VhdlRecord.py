from RecordMember import RecordMember

class VhdlRecord:
    def __init__(self, name : str, direction : str):
        assert isinstance(name, str)
        self.name = name
        assert isinstance(direction, str)
        self.direction = direction
        self.members : RecordMember = []

    def addMember(self, member):
        assert isinstance(member, RecordMember)
        assert False == self.doesMemberExist(member)
        self.members.append(member)

    def doesMemberExist(self, member):
        for mem in self.members:
            if mem.name.strip() == member.name.strip():
                return True
        return False



# from Register import Register
# from RegisterField import RegisterField
#
# reg = Register("register1","rw","0x0")
# field = RegisterField("data","31:16","0")
# field1 = RegisterField("data1","15:0","0")
# reg.addField(field)
# reg.addField(field1)
#
# ins = VhdlRecord("kia")
# ins.addMember(RecordMember("data", reg))
# ins.addMember(RecordMember("data1", reg))
# # ins.addMember({"name" : "data2","width" : 32, "register" : reg})
# # ins.addMember({"name" : "data3","width" : 32, "register" : reg})
# for mem in ins.members:
#     print(mem.name, mem.correspondingRegister.name)
#     for f in mem.correspondingRegister.fields:
#         if f.alias == mem.name:
#             print(mem.msb, mem.lsb)
