from typing import List, Any
import sys
from FieldValue import FieldValue
from RegisterField import RegisterField
from Register import Register
from RegisterFile import RegisterFile
from VhdlCodeGenerator import VhdlCodeGenerator
from CCodeGenerator import CCodeGenerator
from YamlToRegisterFile import YamlToRegisterFile
from VhdlRecord import VhdlRecord
from HtmlDocGenerator import HtmlDocGenerator


assert (2 == len(sys.argv) or 1 == len(sys.argv))

if 1 == len(sys.argv):
    spidr4 = YamlToRegisterFile("../databases/data.yaml")
else:
    path = "../databases/{}".format(sys.argv[1])
    spidr4 = YamlToRegisterFile(path)



cg = VhdlCodeGenerator(spidr4.registerFile, "../templates", "../outputs", "input.vhd", "package.vhd")

cg.generateCode()

ccg = CCodeGenerator(spidr4.registerFile, "../templates", "../outputs", "input.h", "input.c")
ccg.generateCode()

html = HtmlDocGenerator(spidr4.registerFile, "../templates", "../outputs", "input.htm")
html.generateCode()
