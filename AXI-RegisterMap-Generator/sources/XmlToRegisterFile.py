from typing import List, Any
from FieldValue import FieldValue
from RegisterField import RegisterField
from Register import Register
from RegisterFile import RegisterFile
from VhdlCodeGenerator import VhdlCodeGenerator
from CCodeGenerator import CCodeGenerator
import xml.etree.ElementTree as ET

class XmlToRegisterFile:

    def __init__(self, xmlPath : str):
        assert isinstance(xmlPath, str)
        tree = ET.parse(xmlPath)
        root = tree.getroot()
        allRegisters = root.findall("register")
        registers : Register = [None]*len(allRegisters)
        for regCount, reg in enumerate(allRegisters):
            fields : RegisterField = [None]*len(reg.findall("field"))
            for fieldCount, f in enumerate(reg.findall("field")):
                values : FieldValue = [None]*len(f.findall("value"))
                for valCount, val in enumerate(f.findall("value")):
                    values[valCount] = FieldValue(val.get("val"), val.find("description").text)
                fields[fieldCount] = RegisterField(f.get("alias"), f.get("range"), f.get("reset"), values)
            registers[regCount] = Register(reg.get("name"), reg.get("direction"), fields)
        self.registerFile = RegisterFile(root.get("name"), root.get("offset"), registers)
