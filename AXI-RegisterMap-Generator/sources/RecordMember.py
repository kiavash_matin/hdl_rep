from Register import Register
from RegisterField import RegisterField

class RecordMember:
    def __init__(self, name : str, correspondingRegister : Register):
        assert isinstance(name, str)
        self.name = name
        self.correspondingRegister = correspondingRegister
        [self.msb, self.lsb] = self.getMsbLsb

    @property
    def getMsbLsb(self):
        for f in self.correspondingRegister.fields:
            if self.name == f.alias:
                range = f.range.split(":")
                if len(range) > 1:
                    return [f.range.split(":")[0],f.range.split(":")[1]]
                else:
                    return [f.range.split(":")[0],f.range.split(":")[0]]
