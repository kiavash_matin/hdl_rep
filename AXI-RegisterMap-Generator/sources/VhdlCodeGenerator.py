import math
from jinja2 import Environment, FileSystemLoader
from FieldValue import FieldValue
from RegisterField import RegisterField
from Register import Register
from RegisterFile import RegisterFile
from VhdlRecord import VhdlRecord


class VhdlCodeGenerator:

    def __init__(self, registerFile : RegisterFile, inputDir : str, outputDir : str, inputFile : str, pkgInputFile ):

        assert isinstance(registerFile, RegisterFile)
        self.registerFile = registerFile
        self.locAddressSize = int(math.ceil(math.log(registerFile.numberOfRegisters, 2))-1)
        self.addressSize = int(math.ceil(math.log(registerFile.numberOfRegisters*4, 2)))
        assert isinstance(inputDir, str)
        file_loader = FileSystemLoader(inputDir)
        env = Environment(loader=file_loader)
        assert isinstance(inputFile, str)
        self.template = env.get_template(inputFile)
        assert isinstance(outputDir, str)
        self.outputDir = outputDir
        assert isinstance(pkgInputFile, str)
        self.pkgInputFile = pkgInputFile
        self.pkgTemplate = env.get_template(pkgInputFile)



    def generateCode(self):

        ios = []
        signals = []
        connections = []
        resets = []
        wrProcesses = []
        latches = []
        sensitivities = []
        rdProcesses = []
        records = []
        recordString = ""


        for idx, reg in enumerate(self.registerFile.registers):
            signals.append("signal {}_reg\t: std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);".format(reg.name))
            if reg.direction == "rw":
                resets.append("{}_reg\t <= (others => '0');".format(reg.name))
            sensitivities.append("{}_reg".format(reg.name))
            rdProcesses.append("when b\"{}\"\t => \n\t\t\treg_data_out\t<= {}_reg;".format(bin(int(int(reg.offset, 16)/4))[2:].zfill(self.locAddressSize+1), reg.name))
            if reg.direction == "r":
                if reg.record == "x":
                    ios.append("{}\t: in std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);".format(reg.name))
                    connections.append("{}_reg\t<= {};".format(reg.name, reg.name))
            elif reg.direction == "rw":
                if reg.record == "x":
                    ios.append("{}\t: out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);".format(reg.name))
                    connections.append("{}\t<= {}_reg;".format(reg.name, reg.name))
                wrProcesses.append("when b\"{}\"\t=>\n\t\t\t\tfor byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop\n\t\t"
                                   "\t\t\tif ( S_AXI_WSTRB(byte_index) = '1' ) then\n"
                                   "\t\t\t\t\t\t"
                                   "{}_reg(byte_index*8+7 downto byte_index*8) <= S_AXI_WDATA(byte_index*8+7 downto byte_index*8);"
                                   "\n\t\t\t\t\tend if;\n\t\t\t\tend loop;"
                                   .format(bin(int(int(reg.offset, 16)/4))[2:].zfill(self.locAddressSize+1), reg.name))
                latches.append("{}_reg\t<= {}_reg;".format(reg.name, reg.name))

        for rec in self.registerFile.records:
            recordString += "\ttype {} is record".format(rec.name)
            if rec.direction == "r":
                ios.append("{}_register\t: in {};".format(rec.name, rec.name))
                for mem in rec.members:
                    if (int(mem.msb) - int(mem.lsb)) == 0:
                        connections.append("{}_reg({})\t<= {}_register.{};".format(mem.correspondingRegister.name, mem.msb, rec.name, mem.name))
                    else:
                        connections.append("{}_reg({} downto {})\t<= {}_register.{};".format(mem.correspondingRegister.name, mem.msb, mem.lsb, rec.name, mem.name))
            elif rec.direction == "rw":
                ios.append("{}_register\t: out {};".format(rec.name, rec.name))
                for mem in rec.members:
                    if (int(mem.msb) - int(mem.lsb)) == 0:
                        connections.append("{}_register.{}\t<= {}_reg({});".format(rec.name, mem.name, mem.correspondingRegister.name, mem.msb))
                    else:
                        connections.append("{}_register.{}\t<= {}_reg({} downto {});".format(rec.name, mem.name, mem.correspondingRegister.name, mem.msb, mem.lsb))
            for mem in rec.members:
                if (int(mem.msb) - int(mem.lsb)) == 0:
                    recordString += "\n\t\t{}\t: std_logic;".format(mem.name)
                else:
                    recordString += "\n\t\t{}\t: std_logic_vector({} downto {});".format(mem.name, int(mem.msb) - int(mem.lsb), 0)
            recordString += "\n\tend record {};\n".format(rec.name)
            records.append(recordString)
            recordString = ""

        pkgName = "{}_pkg".format(self.registerFile.name)
        isPkg = (len(records) > 0)
        output = self.template.render(isPkg=isPkg, pkgName=pkgName, addressSize=self.addressSize, entityName=self.registerFile.name, ios=ios,
                                      locAddressSize=self.locAddressSize,
                                      numRegisters=self.registerFile.numberOfRegisters, signals=signals,
                                      connections=connections, resets=resets, wrProcesses=wrProcesses,
                                      latches=latches, sensitivities=sensitivities, rdProcesses=rdProcesses)


        vhdlFilePath = "{}/{}.vhd".format(self.outputDir, self.registerFile.name)
        f = open(vhdlFilePath, "w+")
        f.write(output)
        f.close()
        print("VHDL code generated successfully..!")


        pkgOutput = self.pkgTemplate.render(name=pkgName, records=records)
        pkgFilePath = "{}/{}_pkg.vhd".format(self.outputDir, self.registerFile.name)
        f = open(pkgFilePath, "w+")
        f.write(pkgOutput)
        f.close()
        print("VHDL packge generated successfully..!")
