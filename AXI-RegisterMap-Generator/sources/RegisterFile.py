from Register import Register
from VhdlRecord import VhdlRecord


class RegisterFile:

    def __init__(self, name, baseAddress):
        assert isinstance(name, str)
        self.name = name
        assert isinstance(baseAddress, str)
        assert int(baseAddress, 16)
        self.baseAddress = baseAddress
        self.registers : Register = []
        self.records : VhdlRecord = []


    def addRecord(self, record: VhdlRecord, member):
        assert isinstance(record, VhdlRecord)
        rec = self.findRecord(record)
        if  None == rec:
            record.addMember(member)
            self.records.append(record)
        else:
            rec.addMember(member)


    def findRecord(self, record: VhdlRecord):
        for rec in self.records:
            if rec.name.strip() == record.name.strip():
                return rec
        return None

    def isOffsetConfilt(self, register: Register):
        for reg in self.registers:
            if reg.name == register.name:
                print(reg.name, register.name)
                return True
        return False

    def addRegister(self, register : Register):
        assert isinstance(register, Register)
        #assert False == self.isOffsetConfilt(register)
        self.registers.append(register)

    @property
    def getMaxOffset(self):
        max = 0
        i = 0
        while i < len(self.registers):
            if max < int(self.registers[i].offset, 16):
                max = int(self.registers[i].offset, 16)
            i += 1
        return max

    @property
    def isRegistersEmpty(self):
        if 0 == len(self.registers):
            return True
        else:
            return False

    @property
    def numberOfRegisters(self):
        return int(self.getMaxOffset/4) + 1

    @property
    def numberOfReadOnlyRegisters(self):
        count = 0
        for reg in self.registers:
            if reg.direction == "r":
                count += 1
        return count

    @property
    def numberOfReadWriteRegisters(self):
        count = 0
        for reg in self.registers:
            if reg.direction == "rw":
                count += 1
        return count

    @property
    def numberOfUnusedRegisters(self):
        return self.numberOfRegisters - (self.numberOfReadOnlyRegisters + self.numberOfReadWriteRegisters)
