#ifndef _spidr4_register_map_
#define _spidr4_register_map_

#define OFFSET	0x43c00000
#define TRIGGERCONTROL	0x43c00000 
#define TRIGGERFREQUENCY	0x43c00004 
#define TRIGGERPERIOD	0x43c00008 
#define EXTERNALTRIGGERCOUNTER	0x43c0000c 
#define NEWTRIG	0x43c00014 
#define EXTERNALSHUTTERCOUNTER	0x43c00030 
#define FPGACORETEMPERATURE	0x43c00010  

//read functions 
int getTriggercontrol(void);
int getTriggerfrequency(void);
int getTriggerperiod(void);
int getExternaltriggercounter(void);
int getNewtrig(void);
int getExternalshuttercounter(void);
int getFpgacoretemperature(void);

//write functions 
void setTriggercontrol(int);
void setTriggerfrequency(int);
void setTriggerperiod(int);
void setNewtrig(int);
void setFpgacoretemperature(int);

#endif