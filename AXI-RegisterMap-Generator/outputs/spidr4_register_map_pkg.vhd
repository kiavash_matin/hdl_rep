library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package spidr4_register_map_pkg is
 
	type trigger_config is record
		triggerMode	: std_logic_vector(2 downto 0);
		ResetTriggerCounter	: std_logic;
		ResetShutterCounter	: std_logic;
		ResetT0	: std_logic;
		StartAutoTrigger	: std_logic;
		StopAutoTrigger	: std_logic;
		AutoTriggerBusy	: std_logic;
		Enablecounterselpulse	: std_logic;
		InhibitTrigger	: std_logic;
		EnablePowerPulsing	: std_logic;
		EnableMonitorPackeTrigger	: std_logic;
		ResetShutterInhibitCounter	: std_logic;
		Reserved	: std_logic_vector(17 downto 0);
		TriggerFrequency	: std_logic_vector(31 downto 0);
		TriggerPeriod	: std_logic_vector(31 downto 0);
	end record trigger_config;

 
	type trigger_prob is record
		externalTriggerCounter	: std_logic_vector(31 downto 0);
		externalShutterCounter	: std_logic_vector(31 downto 0);
	end record trigger_prob;


end spidr4_register_map_pkg;


