#include "spidr4_register_map.h"

//read functions 
int getTriggercontrol(void)
{
	int* triggerControl = (*int) TRIGGERCONTROL;
	return *triggerControl;
} 
int getTriggerfrequency(void)
{
	int* triggerFrequency = (*int) TRIGGERFREQUENCY;
	return *triggerFrequency;
} 
int getTriggerperiod(void)
{
	int* triggerPeriod = (*int) TRIGGERPERIOD;
	return *triggerPeriod;
} 
int getExternaltriggercounter(void)
{
	int* externalTriggerCounter = (*int) EXTERNALTRIGGERCOUNTER;
	return *externalTriggerCounter;
} 
int getNewtrig(void)
{
	int* newTrig = (*int) NEWTRIG;
	return *newTrig;
} 
int getExternalshuttercounter(void)
{
	int* externalShutterCounter = (*int) EXTERNALSHUTTERCOUNTER;
	return *externalShutterCounter;
} 
int getFpgacoretemperature(void)
{
	int* fpgaCoreTemperature = (*int) FPGACORETEMPERATURE;
	return *fpgaCoreTemperature;
} 

//write functions 
void setTriggercontrol(int value)
{
	int* triggerControl = (int*) TRIGGERCONTROL;
	*triggerControl = value;
} 
void setTriggerfrequency(int value)
{
	int* triggerFrequency = (int*) TRIGGERFREQUENCY;
	*triggerFrequency = value;
} 
void setTriggerperiod(int value)
{
	int* triggerPeriod = (int*) TRIGGERPERIOD;
	*triggerPeriod = value;
} 
void setNewtrig(int value)
{
	int* newTrig = (int*) NEWTRIG;
	*newTrig = value;
} 
void setFpgacoretemperature(int value)
{
	int* fpgaCoreTemperature = (int*) FPGACORETEMPERATURE;
	*fpgaCoreTemperature = value;
} 