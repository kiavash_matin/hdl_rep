#include <stdint.h>

typedef volatile uint32_t	reg_io_t;



// Physical memory addresses
#define OFFSET	{{offset}}{% for reg in regs %}{% for key, value in reg.items() %}
#define {% filter upper %}{{key}}_ADDR{% endfilter %}	0x{{value}} {% endfor %}{% endfor %} 

// Masks {% for mask in masks %}{% for key, value in mask.items() %}
#define {%filter upper %}SPIDR4_{{key}}{% endfilter %}_mask	0x{{value}} {% endfor %}{% endfor %}

// Shifts {% for shift in shifts %}{% for key, value in shift.items() %}
#define {% filter upper %}SPIDR4_{{key}}{% endfilter %}_shift	{{value}} {% endfor %}{% endfor %}

// Values {% for action in actions %}{% for key, value in action.items() %}
#define {% filter upper %}SPIDR4_{{key}}{% endfilter %}	0x{{value}} {% endfor %}{% endfor %}

// Micros {% for micro in micros %}
#define {% filter upper %}SPIDR4_{{micro}}{% endfilter %}(V)	 (((V) << {% filter upper %} SPIDR4_{{micro}}{% endfilter %}_shift) & {% filter upper %}SPIDR4_{{micro}}{% endfilter %}_mask )
{% endfor %}


// Registers struct
struct {{name}}
	{ {% for reg in regs %}{% for key, value in reg.items() %}
		reg_io_t	{% filter upper %}{{key}}{% endfilter %}; {% endfor %}{% endfor %}
	}; 

