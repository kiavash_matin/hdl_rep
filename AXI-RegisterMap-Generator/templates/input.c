#include "{{includeName}}"

//read functions {% for readFun in readFuns %}
int get{% filter capitalize %}{{readFun}}{% endfilter %}(void)
{
	int* {{readFun}} = (*int) {% filter upper %}{{readFun}}{% endfilter %};
	return *{{readFun}};
} {% endfor %}

//write functions {% for writeFun in writeFuns %}
void set{% filter capitalize %}{{writeFun}}{% endfilter %}(int value)
{
	int* {{writeFun}} = (int*) {% filter upper %}{{writeFun}}{% endfilter %};
	*{{writeFun}} = value;
} {% endfor %}
